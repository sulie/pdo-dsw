<?php
    if(!isset($_GET['name'])){
        die("Falta el nombre del parámetro");
    }
    require_once("pdo.php");

    $error = "";
    $mensaje = "";

    if($_SERVER['REQUEST_METHOD']==='POST'){
        if (isset($_POST['km']) && isset($_POST['anio']) && isset($_POST['marca'])) {
            if(is_numeric($_POST['km']) && is_numeric($_POST['anio'])) {

                if(!empty($_POST['marca'])){
                
                    //INSERT
                    $stmt = $pdo->prepare('INSERT INTO autos (make, year, mileage) VALUES ( :mk, :yr, :mi)');
    
                    $stmt->execute(array(
                    ':mk' => htmlspecialchars($_POST['marca']),
                    ':yr' => htmlspecialchars($_POST['anio']),
                    ':mi' => htmlspecialchars($_POST['km']))
                    );
    
                    $mensaje = "Registro insertado";
                }
                else {
                    $error = "La marca no puede estar vacía";
                }
    
            }
            else {
                $error = "Kilómetros y Año han de ser numéricos";
            }
        }
        if (isset($_POST['id'])) {
            $sql = "DELETE FROM autos WHERE auto_id = :id";

            $stmt = $pdo->prepare($sql);
            $stmt->execute(array(':id'=>$_POST['id']));

            $mensaje = "Registro eliminado";
        }
    }
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <header>
            <h1>Autos Admin</h1>
            <a href="index.php">Cerrar sesión</a>
        </header>
        <form action=<?= "autos.php?name=".$_GET['name']?> method="POST">
            <label for="marca">Marca:</label><br>
            <input type="text" name="marca">

            <br>

            <label for="km">Kilómetros:</label><br>
            <input type="text" name="km">

            <br>

            <label for="anio">Año:</label><br>
            <input type="text" name="anio">

            <br><br>

            <input type="submit" value="Añadir">
        </form>

        <?= $error ?>
        <?= "<p style='color: green'>".$mensaje."</p>" ?>
        
        <?php
        $stmt = $pdo->query("SELECT * FROM autos");
        ?>
        <table>
            <tr>
                <th>Marca</th>
                <th>Año</th>
                <th>Km</th>
            </tr>
        <?php
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            ?>
            <tr>
                <td><?= $row['make']?></td>
                <td><?= $row['year']?></td>
                <td><?= $row['mileage']?></td>
                <td>
                    <form action=<?= "autos.php?name=".$_GET['name'] ?> method="POST">
                        <input type="hidden" value=<?= $row['auto_id'] ?> name="id">
                        <input type="submit" value="Borrar">    
                    </form>
                </td>
            </tr>
            <?php
        }
        ?>
        </table>
    </body>
</html>