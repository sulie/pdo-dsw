<?php
function validateForm(){
    if($_SERVER['REQUEST_METHOD']==='POST'){
        if(empty($_POST['email']) || empty($_POST['pass'])){
            echo "<p>El email y la contraseña son campos obligatorios</p>";
        }
        if(!empty($_POST['email']) && strpos($_POST['email'],"@") === false) {
            echo "<p>El email debe contener el símbolo @</p>";
        }
    }
}

function logIn(){
    $salt = 'XyZzy12*_';
    $stored_hash = '1a52e17fa899cf40fb04cfc42e6352f1';

    if($_SERVER['REQUEST_METHOD']==='POST'){
        if(empty($_POST['pass'])==false){
            $password = $_POST['pass'];
            $md5 = hash('md5',$salt.$password);

            if($md5 == $stored_hash){
                error_log("Login success ".$_POST['email']);
                header("Location: autos.php?name=".urlencode($_POST['email']));
            }
            else {
                error_log("Login fail ".$_POST['email']." $md5");
                return "<p>Contraseña incorrecta</p>";
            }
        }
    }   
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Iniciar sesión</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <header>
            <h1>Por favor, inicie sesión.</h1>
        </header>
        <form action="" method="POST">
            <label for="email">Email: </label><br>
            <input type="text" name="email"><br>

            <br>

            <label for="pass">Contraseña: </label><br>
            <input type="password" name="pass"><br>

            <br>

            <input type="submit" value="Iniciar sesión">
        </form>
        <?php 
        validateForm();
        echo logIn();
        ?>
    </body>
</html>